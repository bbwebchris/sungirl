
  $(window).scroll(function(){
    //女神排行 right aside fixed 
    if($(".th-ranking").length>0){
      $(".th-ranking").fixBanner('.md-ranking','.md-banner');
    }
  }).scroll();

  //女神排行按鈕的動態
  $(".md-ranking >a").hover(function(){
    if($(this).hasClass('on')){ return
    }else{
      $(this).find("span").addClass('fadeInDown');
    }
  },function(){
    if($(this).hasClass('on')){ return
    }else{
      $(this).find("span").removeClass('fadeInDown');
    }
  });

  window.addEventListener("orientationchange",onOrientationchange ,false);
  function onOrientationchange() {
      closeSearchBox();
  }

  function isMobile() {
    try{ document.createEvent("TouchEvent"); return true; }
    catch(e){ return false;}
  }

  // 女神排行bottom banner fixed時 fotter需要做margin-bottom
  $(window).resize(function(){
    var wdth = window.innerWidth;
    var mobileBox = $(".mobileSearch");
    var webBox = $(".webSearch");

    if(!isMobile()){
      switch(true){
        case wdth>1366:
          mobileBox.hide();
          webBox.show();
          break;
        case wdth<=1366:
          mobileBox.show();
          webBox.hide();
          break;
        default:
          break;
      }
    }else{
      webBox.hide();
    }

    if($(".th-fixbotbanner").length>0){
      if(wdth<768){
        $("#footer").css({"margin-bottom":"15%"});
      }else{
        $("#footer").css({"margin-bottom":"0%"});
      }      
    }
  }).resize();

  // 女神排行bottom banner 輪播
  $(".th-fixbotbanner").fixedSliderShow();

  //執行搜尋
  getSearchList("searchListWeb");
  getSearchList("searchListMobi");

  //元助排行搜尋功能
  function getSearchList(o){
    const input = document.getElementById(o);
    input.oninput = function(){
    var self = $(self);
    var list = $(".ranklist li");
    var listLength = $(".ranklist li").length;
    var value = input.value.toLowerCase();

    for( var i = 0; i < listLength ; i++ ){
        var nameList = $(list[i]);
        var nameListText = nameList.find('.pt-text h4').text().toLowerCase();
        nameList.removeClass('active');
        $(".noSearch").hide();
        if(nameListText.indexOf(value)>=0){
            nameList.show();
            nameList.addClass('active');
        }else{
            nameList.hide();
            if($(".ranklist li.active").length <= 0){
              $(".noSearch").show();
            }
        }
      }
    }
  }

  //元助排行搜尋按鍵(mobile)
  function getSearchBox(self){
    var _self = self.getAttribute("data-btn");
    var searchBox = $(".mobileSearch").find("input");
    var searchBtn = $(".searchBtn");
    var closeBtn = $(".closeBtn");
    var wdth = window.innerWidth;
    var rankBtnW = ($(".rankBtn").width()-5)*2;

    if(_self === "search"){
      if(wdth<820){
        searchBox.show();
        searchBox.css({"border":"0px solid #8f8f8f"});
        searchBox.animate({width: rankBtnW + 20,left: -(rankBtnW + 35),borderWidth: 1}, 200);
      }else{
        searchBox.show();
        searchBox.css({"border":"0px solid #8f8f8f"});
        searchBox.animate({width: rankBtnW + 20,left: -(rankBtnW + 35),borderWidth: 1}, 200);
      }
      $(self).hide();
      closeBtn.show();
    }else if(_self === "close"){
      closeSearchBox();
    }
  }

  function closeSearchBox(){
    var searchBox = $(".mobileSearch").find("input");
    var searchBtn = $(".searchBtn");
    var closeBtn = $(".closeBtn");

    searchBox.hide();
    searchBox.animate({width: "0px",left: "-355px", borderWidth: 0}, 200);
    closeBtn.hide();
    searchBtn.show();
  }




