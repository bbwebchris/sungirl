  //videoCont切換youtube及cdn
  function switchSrc(el,yk,cdn){
    if(cdn!=null && cdn!=""){
      // cdn
      $("#vdEl source")[0].src=el.data('src');
      $("#vdEl").addClass("show").get(0).load();
      $("#videoiframe").removeClass("show").attr('src','');      
    }else{
      // youtube
      $("#videoiframe").attr('src',el.data('src')).addClass("show"); 
      $("#vdEl").removeClass("show").get(0).pause();
      $("#vdEl source")[0].src='';
    }
  }  
  function switchVideo(elemt){
    if(elemt.hasClass('active')) return;

    elemt.addClass('active').siblings().removeClass('active');
    if(elemt.attr("id")=="youtubeBtn"){
      $("#videoiframe").attr('src',elemt.data('src')).addClass("show"); 
      $("#vdEl").removeClass("show").get(0).pause();
      $("#vdEl source")[0].src='';
    }else{
      switchSrc(elemt,null,"src");
    }        
    
  }
  $("#youtubeBtn").on('click',function(event){
    event.stopPropagation();
    switchVideo($(this));
  });
  $("#others").on('click',function(event){
    event.stopPropagation();
    switchVideo($(this));
  });