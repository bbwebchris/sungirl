/**
 * jquery.snow - jQuery Snow Effect Plugin
 *
 * Available under MIT licence
 *
 * @version 1 (21. Jan 2012)
 * @author Ivan Lazarevic
 * @requires jQuery
 * @see http://workshop.rs
 *
 * @params minSize - min size of snowflake, 10 by default
 * @params maxSize - max size of snowflake, 20 by default
 * @params newOn - frequency in ms of appearing of new snowflake, 500 by default
 * @params flakeColor - color of snowflake, #FFFFFF by default
 * @example $.fn.snow({ maxSize: 200, newOn: 1000 });
 */
$.fn.snow = function(options){
	
  var $flake 			= $('<div id="flake"/>').css({'position': 'fixed' ,'z-index':'250', 'top': '-50px'}).append('<img src="images/gold.svg">'),
    documentHeight 	= $(document).height(),
    documentWidth	= $(document).width(),
    defaults		= {
              minSize		: 10,
              maxSize		: 20,
              newOn		: 1300,
              flakeColor	: "transparent"
            },
    options			= $.extend({}, defaults, options);
    
  
  var interval		= setInterval( function(){
    var startPositionLeft 	= Math.random() * documentWidth - 100,
      startOpacity		= 0.5 + Math.random(),
      sizeFlake			= options.minSize + Math.random() * options.maxSize,
      endPositionTop		= documentHeight - 40,
      endPositionLeft		= startPositionLeft - 100 + Math.random() * 200,
      durationFall		= documentHeight * 10 + Math.random() * 5000;
    $flake
      .clone()
      .appendTo('body')
      .css(
        {
          left: startPositionLeft,
          // opacity: startOpacity,
          'font-size': sizeFlake,
          color: options.flakeColor
        }
      )
      .animate(
        {
          top: endPositionTop,
          // left: endPositionLeft,
          // opacity: 0.2
        },
        durationFall,
        'linear',
        function() {
          $(this).remove()
        }
      );
  }, options.newOn);

};






window.fbAsyncInit = function() {
  FB.init({
    appId      : '732391563599373',
    xfbml      : true,
    version    : 'v2.8'
  });
  FB.AppEvents.logPageView();
};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

function shareFB(pTitle,pImg,pHref){
  FB.ui({
    method: 'feed',
    link: pHref,
    title: pTitle,
    picture: pImg 
  },function (response) { });
}

function shareTwitter(pTitle,pHref){
  window.open('http://twitter.com/?status='.concat(encodeURIComponent(pHref)).concat(' ').concat(encodeURIComponent(pTitle)));
}

function shareWeibo(pTitle,pImg,pHref){
  window.open('http://service.weibo.com/share/share.php?url=' + encodeURIComponent(pHref) + '&title=' + encodeURIComponent(pTitle) + '&pic=' + encodeURIComponent(pImg));
}

function shareDate(data){
  var shareData = data;
  var href = document.location.href;
  if(shareData.title == ""){
    shareData.title = "SunGirlBaby上閣樓" 
  }
  $("#fbShare").off('click').on('click',function(e){
    e.preventDefault();
    shareFB(shareData.title,shareData.img,href);
  });
  $("#weiboShare").off('click').on('click',function(e){
    e.preventDefault();
    shareWeibo(shareData.title,shareData.img,href);
  });
  $("#twitterShare").off('click').on('click',function(e){
    e.preventDefault();
    shareTwitter(shareData.title,href);
  });
}

$(function(){ 

  // snow fall
  // $.fn.snow();
  
  // christmas man
  // var positionX = 0;
  // var christmasbgTimer;
  // var christmasTimer;
  // christmasbgTimer = setInterval(function(){
  //   if($(window).innerWidth()>980){
  //     positionX != -500 ? positionX -= 500 : positionX = 0;
  //     $("#christmasMan").css({backgroundPosition: positionX + "px" + " 0"});
  //   }else {
  //     positionX != -300 ? positionX -= 300 : positionX = 0;
  //     $("#christmasMan").css({backgroundPosition: positionX + "px" + " 0"});
  //   }
  // },240);
  // function christmasmanMove(){
  //   if($(window).innerWidth()>980){
  //     $("#christmasMan").css({ top:"50%",left:"100%",}).stop().animate({top:"30%",left:"-520px",},8000);
  //   }else{
  //     $("#christmasMan").css({ top:"50%",left:"100%",}).stop().animate({top:"30%",left:"-320px",},8000);
  //   }
  // }
  // christmasmanMove();
  // christmasTimer = setInterval(christmasmanMove,120000);

  // mobile menu switch button
  //document.documentElement.style.overflowY = 'hidden';
  $(".closePopBtn").click(function(){
    $(".stopPop").hide();
  }) 

  $("#menuBtn").on("mousedown",function () { 
    $('html').toggleClass('leftMenu');
    $('.sungirltv-nonelimit').addClass('leftHide');
  });
  // goTop
  $("#goTop").on("click",function(){$("html,body").stop().animate({scrollTop: 0});});

  //複製網址
  $("#copybabyLink").on("mousedown",function(){
    var textarea = document.createElement("textarea");
    textarea.textContent = document.URL;
    textarea.style.position = "fixed"; 
    document.body.appendChild(textarea);
    textarea.select();
    try {
      var successful = document.execCommand('copy');
      var msg = successful ? 'successful' : 'unsuccessful';
    } catch (err) {
      console.log('unable to copy');
    } finally{
      document.body.removeChild(textarea);
      alert("網址已複製");
    }
  });

  //載入區塊
  //blockImport();
  
  // pink header hover
  if($("#header").hasClass('hd-pink')){
    $(".md-mainUl >li >a").off("mouseenter click").on("click",function(){
      if(window.innerWidth<=1200){
        if($(this).siblings(".md-secondMenu").length>0 && $("html").hasClass('leftMenu')){
          $(this).siblings(".md-secondMenu").toggleClass('show');
          $(this).parent("li").siblings().find(".md-secondMenu").removeClass('show');
        } 
      }
    });
    $(".md-mainUl >li >a").on("mouseenter",function(){
      if(window.innerWidth>1200 && $(this).siblings(".md-secondMenu").length>0){
        if($(window).scrollTop() < $(".main-content").offset().top){
          if($(".main-Index").length>0){
            $(this).siblings(".md-secondMenu").css({top:"100%",height:"auto"}).slideDown(300,function(){
              $(this).stop().animate({top: ($(this).height()-$("#header").height())*-0.5},350);
            });
          }else{
            $(this).siblings(".md-secondMenu").addClass('md-show');
          }
        }else{
          // menu fixed 後 md-secondMenu 只能top 0;
          $(this).siblings(".md-secondMenu").addClass('md-show');
        }
      }else{
        $(this).siblings(".md-secondMenu").removeClass('md-show');
      }
    });
    $(".md-mainUl >li").on("mouseleave",function(){
      if($(this).find(".md-secondMenu").length>0 && window.innerWidth>1200){
        if($(window).scrollTop() < $(".main-content").offset().top){
          $(".md-secondMenu").attr("style","");
        }else{
          $(".md-secondMenu").attr("style","").removeClass('md-show');
        }
      }
    });
  } 

  //blue header hover
  if($("#header").hasClass('hd-blue')){
    // menu click effect
    $(".md-mainUl >li >a").off("click").on("click",function (){
      if($(this).siblings(".md-secondMenu").length==1){
        $(this).siblings(".md-secondMenu").toggleClass('show');
        if($(this).siblings(".md-secondMenu").hasClass('show')){
          $("#header").addClass('secondUl-active');
        }else{
          $("#header").removeClass('secondUl-active');
        }
        $(this).parent("li").siblings().find(".md-secondMenu").removeClass('show');
      }
    });
    $(".md-mainUl .closeBtn").off("click").on("click",function(){
      $(this).parents(".md-secondMenu").toggleClass('show');
      if($("#header").hasClass('secondUl-active')){
        $("#header").removeClass('secondUl-active');
      }
    });

    // document ready each by each add class
    $("#header h1").addClass('show');
    $("#header h1.show").one("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd",function(){
      $("#header h1 img").addClass('imgShow');
      $("#header h1 img.imgShow").one("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd",function(){
        $("#header .md-mainUl >li").addClass('liShow');
      });
    });
    // for fix ie9 (blue menu animate not support ie9)
    if(navigator.userAgent.indexOf("MSIE 9.0")>0) {
      $("#header h1").addClass('show');
      $("#header h1 img").addClass('imgShow');
      $("#header .md-mainUl >li").addClass('liShow');
    }
  }
  // window small than 1200 no animate on nav
  if($("#header").hasClass('hd-blue') && window.innerWidth<1200){
    // document ready add class
    $("#header h1").addClass('show');
    $("#header h1 img").addClass('imgShow');
    $("#header .md-mainUl >li").addClass('liShow');
  }

  //scroll window set header 
  $(window).scroll(function(){
    if(window.innerWidth>1200){
      if($(window).scrollTop()>$(".main-content").offset().top){
        $("#header").addClass('fixedMenu');
        if($(".hd-pink").length>0){$(".main-content").addClass('fixmargin');}
        if($(".hd-blue").length>0){$(".th-commonShare").addClass('fixed');}
        $("#goTop").addClass('show');
      }else{
        $("#header").removeClass('fixedMenu');
        if($(".hd-pink").length>0){$(".main-content").removeClass('fixmargin');}
        if($(".hd-blue").length>0){$(".th-commonShare").removeClass('fixed');}
        $("#goTop").removeClass('show');
      }
    }else{
      $("#header").removeClass('fixedMenu');
      if($(".hd-pink").length>0){$(".main-content").removeClass('fixmargin');}
      if($(".hd-blue").length>0){$(".th-commonShare").removeClass('fixed');}
    }
  }).scroll();

  var menuTimer;
  $(window).on("resize",function(){
    clearTimeout(menuTimer);
    menuTimer = setTimeout(function(){
      if(window.innerWidth <=1200){
        if($("#header").hasClass('hd-pink')){
          $("body").removeClass('leftMenu'); 
          $(".md-secondMenu").attr("style","");
        }
        if($("#header").hasClass('fixedMenu')){
          $("#header").removeClass('fixedMenu')
        }
      }
    },50)    
  }).resize();

  //判斷裝置
  function isMobile() {
    try{ document.createEvent("TouchEvent"); return true; }
    catch(e){ return false;}
  }

  if(!isMobile()){
    $(".md-top-banner").hide();
    $(".md-web").show();
    $(".md-web").find('*').show();
    $(".md-mobile").find('*').hide();
    $(".md-mobile").hide();
  }else{
    $(".md-top-banner").show();
    $(".md-web").find('*').hide();
    $(".md-web").hide();
    $(".md-mobile").find('*').show();
    $(".md-mobile").show();
  }
});

// copy event all website
function addLink() {
  var date = new Date();
  var body_element = document.getElementsByTagName('body')[0];
  var selection = window.getSelection();
  var pagelink = "<br /> - SunGirlbaby <a href='"+document.location.href+"'>"+document.location.href+"</a><br />- Copyright &copy;"+date.getFullYear();; 
  var copytext = selection + pagelink;
  var newdiv = document.createElement('div');
  newdiv.style.position='absolute';
  newdiv.style.left='-99999px';
  body_element.appendChild(newdiv);
  newdiv.innerHTML = copytext;
  selection.selectAllChildren(newdiv);
  window.setTimeout(function() {
    body_element.removeChild(newdiv);
  },0);
}
document.oncopy = addLink;


$.fn.datePicker = function(){
  var element = $(this);
  var el_year = $(this).find(".year");
  var el_month = $(this).find(".month");
  var el_day = $(this).find(".day");
  var now = new Date();
  var now_year = now.getFullYear()-1;
  var yearStr = "";
  var monthStr = "";
  var endDate;
  var smallMonth = [2,4,6,9,11];
  var chooseYear;
  var chooseMonth;

  for(now_year; now_year > 1959; now_year--){
    yearStr += '<option value="'+ now_year +'">'+ now_year +'</option>'
  }
  el_year.append(yearStr);

  for(var i=1; i<13; i++){
    monthStr += '<option value="'+ i +'">'+ i +'</option>';
  }

  el_year.on("change",function(){
    var self = $(this);
    if( (self.val()/1) !== 0 ){
      chooseYear = self.val()/1;
      el_day.find("option").remove();
      el_month.find("option").remove().end().append(monthStr);
    }else{
      el_day.find("option").remove();
      el_month.find("option").remove();
    }
  });

  el_month.on("change",function(){
    chooseMonth = $(this).val()/1;
    var february;
    var dayStr = "";
    if( chooseMonth !== 0 ){
      february = (chooseYear-1960)%4;
      if(smallMonth.indexOf(chooseMonth)===-1){
        endDate = 31;
      }else{
        if(chooseMonth == 2){
          endDate = february==0? 29: 28;
        }else{
          endDate = 30;
        }
      }
      for(var j=1; j<(endDate+1); j++){
        dayStr += '<option value="'+ j +'">'+ j +'</option>';
      }
      el_day.find("option").remove().end().append(dayStr);
    }else{
      el_day.find("option").remove();
    }
  });

  function defaultYear(yy){
    $("#calendar .year option").each(function(i,element){
      if($(this).text() == yy){
        $(this).prop('selected', true);
        el_year.trigger('change');
        $("#calendar .month option[value=1]").prop('selected', true);
        el_month.trigger('change');          
      }
    });
  }  
  defaultYear((now.getFullYear()-20));
  return element;
}
if($("#calendar").length){
  $("#calendar").datePicker();
}

function switchCommonPop(pPoints,pUrl,pTotal){
  if(pTotal!=undefined){
    $(".th-buyNow .pt-pay a").attr('href',pUrl).text('寶貝金：'+pPoints);
    $(".th-buyNow .pt-total").text(pTotal);
  }else{
    if(pPoints!=undefined && pUrl!=undefined){
      $(".th-getNow .pt-pay a").attr('href',pUrl).text('領取寶貝金：'+pPoints);
    }
  }  
  if($("main").hasClass('switchHideBlock')){
    $("main").removeClass("switchHideBlock");
    var time = setTimeout(function(){
      $("html").removeClass("blackMask");
    },500);
  }else{
    $("html").addClass("blackMask");
    $("main").addClass("switchHideBlock");
  }
}

// switch livePop
function switchLivePop(){
  $(".livePop").toggleClass('livePopHide')
}
// switchLivePop()
$(".livePop .closeBtn").on('click',switchLivePop)



// 18歲lightbox
if($('.th-prohibit18').length>0){
  $('html').addClass('blackMask');
  $('.th-prohibit18').addClass('show');
  $('.th-prohibit18 .bt-close').on('click',function(){
    $(this).closest('.th-prohibit18').removeClass('show');
    $('html').removeClass('blackMask');
  });
}

function blockImport(){
  var import_html = document.querySelector('link[rel="import"]');
  // 取得import html的DOM，並取得期中想要插入的標籤節點
  var footer_block = import_html.import.querySelector("#footer");

  // 插入到現有的DOM之中
  //Footer
  document.body.appendChild(footer_block);
  getThisYear();
}

function getThisYear(){
  // 取得今日日期
  var today = new Date();
  //設定年分
  $(".getYear").text(new Date().getFullYear());
}

//判斷是否為行動載具
function isMobile() {
  try{ document.createEvent("TouchEvent"); return true; }
  catch(e){ return false;}
}