// 測試資料-----start
var productData = {
  "status": "success",
  "cartData": {
    "4c6fe6b456c8dfe91b4c5dea67dd0822": {
      "rowId": "4c6fe6b456c8dfe91b4c5dea67dd0822",
      "id": "2001001",
      "name": "DJ Soda 迷你寫真書",
      "quantity": 2,
      "unitPrice": 1600,
      "transportable": true,
      "weight": 1,
      "options": {
        "stock_shipping": 50,
        "main_img": "d002.jpg"
      }
    },
    "41a7baffeb6b1f79fa39a7f928433a00": {
      "rowId": "41a7baffeb6b1f79fa39a7f928433a00",
      "id": "2001002",
      "name": "book2",
      "quantity": 10,
      "unitPrice": 1880,
      "transportable": true,
      "weight": 1,
      "options": {
        "stock_shipping": 100,
        "main_img": "d008.jpg"
      }
    }
  },
  "msg": "加入購物車成功"
}
// 測試資料-----end


// formatPrice
function formatPrice(num,glue){
  if(isNaN(num)) { return NaN;}
  var glue = (typeof glue== 'string') ? glue: ',';
  var integerDigits = num.toString().split("");
  var threeDigits = [];
  var limit = 3;
  while (integerDigits.length > limit) {
      threeDigits.unshift( integerDigits.splice(integerDigits.length - limit, limit).join("") );
  }
  threeDigits.unshift(integerDigits.join(""));
  integerDigits = threeDigits.join(glue);
  return integerDigits;
}


// 購物車清單
function setCart(data){
  var liDom = "";
  var dataArray = [];
  var totalaprice = 0;
  var totalquantity = 0;
  $.each(data.cartData, function( index, obj ) {
    dataArray.push(obj);
  });
  if(dataArray.length>0){
    $(".th-cartList .md-cartCont .emptyCart").addClass('stepHide');
    $(".th-cartList .md-cartCont .shoppingCart").removeClass('stepHide');
    $(".th-cartList .shoppingCart ul li").remove();
    for(var i = 0; i<dataArray.length; i++){
      var optionDom="";
      for(var j = 0; j < dataArray[i].options.stock_shipping ; j++){
        if(j == dataArray[i].quantity){
          optionDom += '<option value="" selected="selected">'+j+'</option>'
        }else{
          optionDom += '<option value="">'+j+'</option>'
        }
      }
      liDom = '<li>'+
                '<img src="images/mall/'+dataArray[i].options.main_img+'" alt="">'+
                '<span class="text" data-title="品項：">'+dataArray[i].name+'</span>'+
                '<span class="price" data-title="單價：">'+ formatPrice(dataArray[i].unitPrice) +'</span>'+
                '<span class="select" data-title="數量：">'+
                  '<select name="" id="">'+optionDom+'</select>'+         
                '</span>'+
                '<span class="total" data-title="小計：">'+ formatPrice((dataArray[i].unitPrice*dataArray[i].quantity)) +'</span>'+
                '<button></button>'
              '</li>'

      $(".th-cartList .shoppingCart ul").append(liDom);
      totalquantity += dataArray[i].quantity;
      totalaprice += dataArray[i].unitPrice*dataArray[i].quantity;
      $(".th-cartList .shoppingCart .totalinfo").text(formatPrice(totalaprice));
      $(".th-cartList .md-head .price").text(formatPrice(totalaprice));
      $(".th-cartList .md-head .quantity").text(totalquantity);
    }
  }else{
    $(".th-cartList .md-cartCont .shoppingCart").addClass('stepHide');
    $(".th-cartList .md-cartCont .emptyCart").removeClass('stepHide');
    $(".th-cartList .shoppingCart .totalinfo").text(0);
    $(".th-cartList .md-head .price").text(0);
    $(".th-cartList .md-head .quantity").text(0);
  }
}

// close button click
$(".th-cartList .shoppingCart li button").on('click',function(){
  $(this).parent().remove();
});

setCart(productData);


// 購物車清單 open
function openCartlist(){
  $("html").addClass("orderMask");
  $("main").addClass("switchOrder");
}

// 購物車清單關閉 true-->reload, flase-->animate close
function closeCartList(status){
  if(!status){
    $("main").removeClass("switchOrder");
    $(".th-cartList").css({"z-index":"160"});
    $(".th-orderMask").css({"z-index":"150"});
    var time = setTimeout(function(){
      $("html").removeClass("orderMask");
      $(".th-cartList").attr("style","");
      $(".th-orderMask").attr("style","");
    },500);
  }else{
    window.location.reload();
  }
}

//切換公司用或個人用發票
function toggleReceiptBox(){
  var _e = $("input[data-id='ei']").is(":checked");
  if(_e){
    $(".pt-payment-email-box").show();
    $(".pt-payment-method-box").addClass('active');
  }else{
    $(".pt-payment-method-box").addClass('active');
    $(".pt-payment-email-box").hide();
  }
}

//切換電子或捐贈發票
function toggleReceipt(){
  var _i = $(".receipt option:selected").attr("data-id");
   
  if(_i == "company"){
    $(".receipt_num").show();
  }else{
    $(".receipt_num").hide();
  }
}

function toggleReceiptBox(){
  var _e = $("input[data-id='ei']").is(":checked");
  if(_e){
    $(".pt-payment-email-box").show();
  }else{
    $(".pt-payment-email-box").hide();
  }
}

//檢查核取方塊
function checkEmail(){
  var _e = $("input[data-id='ei']").is(":checked");
  if(_e){
    if(checkEmailType()<0){
      return false;
    }else{
      return true;
    };
  }
}

//檢查email格式
function checkEmailType(){
  var emailRule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;
  var _v = $("input[type='email']").val();
  var _s = _v.search(emailRule);

  if(_v == "" || _s == -1){
    $("input[type='email']").css("border","1px solid #f00");
    return _s;
  }
}

function sendReceipt(){
  var _e = $("input[data-id='ei']").is(":checked");
  var _d = $("input[data-id='di']").is(":checked");

  if( _e || _d ){
    if((_e && checkEmail()) || _d){
      //ajax送出成功後執行
      $(".pt-payment-method-box").addClass('active');
      $("input[type='email']").css("border","none");
      $(".sendReceiptBtn").hide();
    }
  }else{
    alert('請先選擇發票格式');
  }
}

$(function(){
  $(".th-mallTitle .md-shoppingCart").on("click",openCartlist);
  $(".th-product .md-text .checkout").on("click",openCartlist);
  $(".main-shoppingSteps1 .th-orderProcess .md-head .items").on("click",openCartlist);
  toggleReceipt();
  $(".receipt").on("change",toggleReceipt);

  $("input[name='receipt']").on("click",function(){
    toggleReceiptBox();
    $(".pt-payment-method-box").removeClass('active');
    $(".sendReceiptBtn").show();
  });

  //
  $(".th-cartList .md-close .btn-close").on("click",function(){
    closeCartList(false); //flase-->animate close
    // closeCartList(true); //true-->reload
  });

  $(".th-cartList .shoppingCart li button").on('click',function(){
    $(this).parent().remove();
  });
});

window.onload = function(){
  $("input[name=receipt]").attr("checked",false);
}



