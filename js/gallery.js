$.extend({
    getUrlVars: function(){
    //debugger;
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
    },
    getUrlVar: function(name){
    return $.getUrlVars()[name];
    }
});
function setSize(){
    $("#gallery figure").each(function(i,e){
        var t= $(e);
        t.css({
            paddingTop:(t.data("h")*1)/(t.data("w")*1)*100+"%",
            position:"relative"
        });
    });
    $("img.lazy[src='']").lazyload({
        effect : "fadeIn"
    });
    $(window).resize();
}

var nextPage = 1;
var loading = false;
var ajaxUrl;
$(document).scrollTop(0);

// getContent("data.txt");
function getContent(url) {
    ajaxUrl = url;
    if (loading || nextPage == "end") return;
    ajaxLoad(ajaxUrl);
}

var wfid;
function setlinkwf(){
    var self = $(this);
    var timer;
    wfid = self.parents('.md-wfImg').attr('id');   
    if(self.hasClass('choosed')){
        self.removeClass('choosed');
        if( $(".main-accountInfo").length){
            // accountInfo頁面的like按鈕
            self.removeClass('choosed');
            timer = setTimeout(function(){
                self.parents('.md-wfImg').remove().delay(800).queue(function(){
                $(window).resize();
                clearTimeout(timer);
            });
            },1000);
        } 
    }else{
        if( $(".main-accountInfo").length) return;
        self.addClass('choosed');
    }
}



function ajaxLoad(ajax_url){
    if(nextPage == "end") return false;
    loading = true;
    if($(".main-Index").length>0){
        $("#wf-loading").hide();
    }else{
        $("#wf-loading").show();
    }
    var ajaxData = { p: nextPage };
    if ($.getUrlVar("t") + "" != "undefined") ajaxData.t = $.getUrlVar("t");
    if ($.getUrlVar("k") + "" != "undefined") ajaxData.k = $.getUrlVar("k");
    if ($.getUrlVar("s") + "" != "undefined") ajaxData.k = $.getUrlVar("s");
    
    $.ajax({ type: "POST", dataType: "text", url: ajax_url, data: ajaxData,
        success: function (msg) {
            if (msg != "end") {
                $("#gallery").append(msg);
                if (nextPage == 1) {
                    init();
                }else{
                    setSize();
                }
                nextPage += 1;
            } else {
                nextPage = msg;
                $("#wf-finished").parent().addClass("show");
            }
            loading = false;
            if($(".main-Index").length==0){
                $("#wf-loading").hide();
            }
            // link click
            $("#gallery .pt-like").off('mousedown').on('mousedown', setlinkwf);
            
        },
        error: function () {
            alert("Error");
            loading = false;
            if($(".main-Index").length==0){
                $("#wf-loading").hide();
            }
        }
    })
}
function init() {
    setSize();
    $("#gallery").initACWF();
    $(window).resize(function() {
        $("#gallery").setACWF();
    }).resize();
    
    $(window).scroll(function() {
        if ($(window).scrollTop() >= ($(document).height() - $(window).height() -$("#footer").height()) && !loading) {
            ajaxLoad(ajaxUrl);
        }
    });
}
