$(function(){
  var timer;
  function dwContReset(){
    // 重置.infoShow的內容高度
    $(".th-download .detailinfo.infoShow").css({height: $(".th-download .infoShow .monthCont").outerHeight() });   
    //download內容重置left位置 
    var dwCont = $(".th-download .md-month");
    dwCont.each(function(){
      $(this).find(".detailinfo").css({left:""});
      if(window.innerWidth>980){
        $(this).find(".detailinfo").css({left: ($(this).index()%3)*100*(-1)+"%"});
      }
      if(window.innerWidth>640 && window.innerWidth<=980){
        $(this).find(".detailinfo").css({left: ($(this).index()%2)*100*(-1)+"%"});
      }      
      if(window.innerWidth<640){
        $(this).find(".detailinfo").css({left: "0%"});
      }
    });
  }

  $(".th-download .md-month").on("mousedown",function(){ 
    var selfe = $(this);
    var lineNum, infoNum;
    var timer;
    if(window.innerWidth>980){
      lineNum = ~~((selfe.index()/3));
      if(selfe.parent().find('.infoShow').length>0){
        infoNum = ~~(selfe.parent().find('.infoShow').parent().index()/3) ;
      }
    }  
    if(window.innerWidth>640 && window.innerWidth<=980){
      lineNum = ~~((selfe.index()/2));
      if(selfe.parent().find('.infoShow').length>0){
        infoNum = ~~(selfe.parent().find('.infoShow').parent().index()/2) ;
      }
    }
    if(selfe.find('.infoShow').length>0){
      // 按已開的則關閉
      selfe.removeClass('open').find('.detailinfo').removeClass('infoShow').css({height:"0px",transition:'0.4s'});
    }else{
      // 同一層
      if(lineNum == infoNum){
        if(window.innerWidth>640){
          selfe.siblings().removeClass('open').find('.detailinfo').removeClass('infoShow').css({height:"",transition:'0s'});
          selfe.addClass('open').find('.detailinfo').addClass('infoShow').css({
            height:selfe.find('.monthCont').outerHeight(),
            transition:'0s'
          });
        }else{
          // 單張
          selfe.siblings().removeClass('open').find('.detailinfo').removeClass('infoShow').css({height:"",transition:'0.4s'});
          selfe.addClass('open').find('.detailinfo').addClass('infoShow').css({
            height:selfe.find('.monthCont').outerHeight(),
            transition:'0.4s'
          });
          timer=setTimeout(function(){
            $("html,body").stop().animate({scrollTop: selfe.find('.monthCont').offset().top-selfe.find('.defaultinfo').height()*0.3},300);
          },410,function(){clearTimeout(timer);});  
        }
      }else{
        // 一般狀況
        selfe.siblings().removeClass('open').find('.detailinfo').removeClass('infoShow').css({height:"",transition:'0.4s'});
        selfe.addClass('open').find('.detailinfo').addClass('infoShow').css({height:selfe.find('.monthCont').outerHeight(),transition:'0.4s'});
        if(window.innerWidth>1200){
          timer=setTimeout(function(){
            $("html,body").stop().animate({scrollTop: selfe.find('.monthCont').offset().top-selfe.find('.defaultinfo').height()*0.3-100},300);  
          },410,function(){clearTimeout(timer);});
        }else{
          timer=setTimeout(function(){
            $("html,body").stop().animate({scrollTop: selfe.find('.monthCont').offset().top-selfe.find('.defaultinfo').height()*0.3},300);
          },410,function(){clearTimeout(timer);});
        }      
      }     
    }
    // 阻擋event fllow
    selfe.find(".monthCont").on("mousedown",function(event){event.stopPropagation();});
  });

  $(window).on("resize",function(){
    clearTimeout(timer);
    timer = setTimeout(dwContReset, 50);
  }).resize();

  $("img.lazy").lazyload();
  

});