  // 元助女神---------------------------start----------------
  $("#th-sponsor").on('click',closePop);
  $("#th-sponsor .md-sponsor").on('click',function(event){event.stopPropagation();});
  $("#th-sponsor .pt-payment-box").on('click',function(event){event.stopPropagation();});
  $(".closeBtn").on('click',closePop);
  $("#th-loginCont2").on('mousedown',function(){$("html").removeClass('loginShow')});
  $("#th-loginCont2 .gp-mdCont").on('mousedown',function(event){event.stopPropagation();});
  $(".pt-pay").find("a").mousedown(function(e){if(e.button ==2){closePop()}});
  // 元助女神----------------------------end-----------------
    //切換公司用或個人用發票
  function toggleReceiptBox(){
    var _e = $("input[data-id='ei']").is(":checked");
    if(_e){
      $(".pt-payment-email-box").show();
    }else{
      $(".pt-payment-email-box").hide();
    }
  }

  //檢查核取方塊
  function checkEmail(){
    var _e = $("input[data-id='ei']").is(":checked");
    if(_e){
      if(checkEmailType()<0){
        return false;
      }else{
      	return true;
      };
    }
  }

  //檢查email格式
  function checkEmailType(){
    var emailRule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;
    var _v = $("input[type='email']").val();
    var _s = _v.search(emailRule);

    if(_v == "" || _s == -1){
      $("input[type='email']").css("border","1px solid #f00");
      return _s;
    }
  }

  function sendReceipt(){
  	var _e = $("input[data-id='ei']").is(":checked");
  	var _d = $("input[data-id='di']").is(":checked");

  	if( _e || _d ){
  		if((_e && checkEmail()) || _d){
  			//ajax送出成功後執行
  			$(".md-sponsor").addClass('active');
  			$("input[type='email']").css("border","none");
  			$(".sendReceiptBtn").hide();
  		}
  	}else{
  		alert('請先選擇發票格式');
  	}
  }

  function closePop(){
    $("html").removeClass('sponsorShow');
    $("input[name='receipt']").attr("checked",false); 
    $(".pt-payment-email-box").hide();
    $(".md-sponsor").removeClass('active');
    $(".donatemoney").find("input").val("");
    $(".pt-payment-title").find("b").text("0");
    $(".pt-payment-tips").text("點擊完成支付");
  }

  function setPrice(){
    var _p = parseInt($(".donatemoney").find("input").val());
    var _wallet = parseInt($(".pt-deposit-box .userPoint").text().replace(/,/g, ""));
    if(_p){
      $(".pt-payment-title").find("b").text(_p);
      if( _p > _wallet){
        $(".pt-payment-tips").text("點數不足,點擊前往儲值");
      }
    }
  }