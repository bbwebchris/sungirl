
  $(document).ready(function runOnce(fn, context) {
    

    
    $("html").addClass('sponsorShow'); 
    once = undefined
  });

  


$.fn.productPop = function(){
  var element = $(this),
      bigImg = element.find(".md-img figure"),
      smallLi = element.find(".md-img .pt-smallimg li"),
      bigimgUl = element.find(".md-fullScreen .pt-img"),
      imgNumber = element.find(".md-fullScreen .pt-imgNumber"),
      leftBtn = element.find(".md-fullScreen .leftBtn"),
      rightBtn = element.find(".md-fullScreen .rightBtn"),
      closeBtn = element.find(".md-fullScreen .pt-closebtn"),
      firstOpenStatus = false,
      imgNow = 0,
      imgNext = null,
      imgMax = smallLi.length;

  // clone smllimg src to fullScreen img src
  smallLi.eq(imgNow).addClass("on");
  bigimgUl.append(smallLi.clone()).find(".on").css({opacity:1}).siblings().css({opacity:0});

  smallLi.on("click",function(){
    var imgSrc = $(this).find("img").attr("src");
    imgNow = $(this).index();
    $(this).addClass("on").siblings().removeClass("on");
    // change src of big img
    $(this).parent().siblings("figure").find("img").attr("src",imgSrc);
    // change index of img in pop img
    bigimgUl.children().eq(imgNow).css({opacity:1}).addClass("on").siblings().css({opacity:0}).removeClass("on");
  });

  function fadinImg(num){
    var spanEl1;
    var spanEl2 = document.createElement("span");
    var goTop = false;
    var numberNow;
    imgNext = num;
    goTop = imgNext > imgNow ? true : false ;
    imgNext = imgNext > imgNow ? imgNext%imgMax : (imgNext+imgMax)%imgMax; 
    numberNow = (imgNext+1)%imgMax != 0 ? (imgNext+1)%imgMax : imgMax ;
    if(firstOpenStatus){
      imgNumber.find(".imgTotle").text(imgMax);
      spanEl1 = document.createElement("span");
      imgNumber.find(".imgNow").append(spanEl1);
      $(spanEl1).text(imgNext+1).css({"top":"0%","opacity":1});

    }
    bigimgUl.children().eq(imgNow).removeClass("on").stop().animate({opacity:0},500);
    bigimgUl.children().eq(imgNext).addClass("on").stop().animate({opacity:1},500,function(){
      imgNow = imgNext;
      imgNext=null;
    });   

    if(goTop){
      imgNumber.find(".imgNow").append(spanEl2);
      $(spanEl2).text(numberNow).css({"top":"100%","opacity":0});
      imgNumber.find(".imgNow").children().eq(0).css({"top":"0%","opacity":1}).stop().animate({"top":"-100%","opacity":0},400);
    }else{
      imgNumber.find(".imgNow").prepend(spanEl2);
      $(spanEl2).text(numberNow).css({"top":"-100%","opacity":0});
      $(spanEl2).siblings().css({"top":"0%","opacity":1}).stop().animate({"top":"100%","opacity":0},400);
    }
    $(spanEl2).stop().animate({"top":"0%","opacity":1},400,function(){
      $(spanEl2).siblings().remove();
    });    

  }

  leftBtn.on("click",function(){
    if(imgNext == null && imgMax>1){
      firstOpenStatus = false;
      imgNext = imgNow - 1;
      fadinImg(imgNext);
    }
  });
  rightBtn.on("click",function(){
    if(imgNext == null && imgMax>1){
      firstOpenStatus = false;
      imgNext = imgNow + 1;
      fadinImg(imgNext);
    }
  });
  

  bigImg.on("click",function(){
    if(imgMax>0){
      $("body").addClass("hidden");
      firstOpenStatus = true;
      fadinImg(element.find(".pt-smallimg .on").index());
      var mdfull = $(this).parent().siblings(".md-fullScreen");
      mdfull.css({"z-index":"105"}).find(".pt-img").children().eq();
      $(this).parent().siblings(".md-mask").addClass("show").css({"z-index":"101"});
      var timer = setTimeout(function(){
        mdfull.addClass("show");
      },350);
    }
  });

  closeBtn.on("click",function(){
    var mask = $(this).parent().siblings(".md-mask");
    var parent = $(this).parent();
    parent.removeClass("show");
    mask.removeClass("show");
    var timer = setTimeout(function(){
      imgNumber.find(".imgNow").children().remove();
      imgNumber.find(".imgTotle").text("");
      parent.attr("style","");
      mask.attr("style","");
      $("body").removeClass("hidden");
    },490);
  });

  return element;
}



$(function(){
  // 商品pop open
  $(".js-productPop").productPop();


  // 加到購物車按鈕
  var cartStatus = true;
  function addCart(e){
    if(cartStatus){
      var _this = $(this);
      var spanText = _this.text();      
      cartStatus = false;
      _this.addClass("active");
      _this.find("span").text(_this.data("text"));
      _this.data("text",spanText);
      var timer = setTimeout(function(){
        if(!cartStatus){
          cartStatus = true;
          spanText = _this.text();
          _this.removeClass("active");
          _this.find("span").text(_this.data("text"));
          _this.data("text",spanText);   
        }
      },1000)      
    }
  }
  $(".th-product .addtoCart").on("click",addCart);

});

    $('#btn').click();
    $('#btn').trigger("click");
    
    
    