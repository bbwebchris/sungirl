$(document).ready(function(){

  $(".landingNav").find(".title").click(function(){
    var t = $(this).siblings(".landingSubmenu").css("display");
    if(t == "none"){
      $(this).siblings(".landingSubmenu").animate({height:"100%"}, 500);
      $(this).siblings(".landingSubmenu").css("display","block");
      
    }else{
      $(this).siblings(".landingSubmenu").css("height","0");
      $(this).siblings(".landingSubmenu").css("display","none");
    }
  })

  $(".menu_btn").click(function(){
    $(".landingNav").animate({height:"100%"}, 300);
    $(".landingNav").css("display","block");
    $(".landingMenu").hide();
  })

  $(".closeBtn").click(function(){
    $(this).parent().animate({height:"0"}, 300);
    $(this).parent().css("display","none");
    $(".landingMenu").show();
  })

  $(".itemLink").click(function(){
    var _a = $(this).find("a").attr("href");
    window.location = _a;
  })

  setSlider();
  dayBgChange();
  getContent();
})

function setSlider(){
  var _w = $(window).width();
  var _f ;
  switch(true){
    case (_w<=840):
      _f = 1;
      break;
    case (_w<=1366):
      _f = 4;
      break;
    default:
      _f = 5;
      break;
  }

  $('.landingNewsSlider').slick({
      slidesToShow: _f,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      asNavFor: "#thumbnail_slider"
    });
    $("#thumbnail_slider").slick({
      slidesToShow: _f,
      slidesToScroll: 1,
      autoplay: true,  
      asNavFor: "#slider",
      arrows: false
    });

}

function getContent() {
  ajaxLoad();
}

var nextPage = 1;
var lastDate;
var loading = false;

//與後端取得新聞內容
function ajaxLoad(){
  let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  let ajaxData = { p: nextPage, _token: CSRF_TOKEN };
  let newsDate = new Array();
  let newsData;
  
  loading = true;
  //textUrl = ["http://www.mocky.io/v2/5e3787543100006c00d37ad7","http://www.mocky.io/v2/5e37877b3100006c00d37ad8","http://www.mocky.io/v2/5e3787a03100004c00d37ad9","http://www.mocky.io/v2/5e3780833100006c00d37acc","http://www.mocky.io/v2/5e3a28c72f0000820056c158"]   
  //textUrl = ["http://www.mocky.io/v2/5e3bd3a53000000d2c21477a"];
  $.ajax({ type: "POST", dataType: "json", url: '/ajax/getLandingList', data: ajaxData,
    success: function (msg) {
      
      newsData = msg.data.data;
      if(newsData.length === 0) {
        $(".lds-ellipsis").remove(); 
        return false;
      }
      init();
      nextPage += 1;
      loading = false;
      ary = new Array();

      for (i in newsData){
        newsDate.push(msg.data.data[i]["ready_time"]);
        let _dateCode = msg.data.data[i]["ready_time"].replace(/-/g, "/");
        let _date = new Date(_dateCode);
        let obj = _date.getFullYear() + "/" + (_date.getMonth()+1) + "/" + _date.getDate();
        if(obj !== lastDate){
          lastDate = obj;
          $(".lds-ellipsis").remove();
          appendDate(obj,_date);
        }
      }

      for(x in newsData) ary[ary.length]=x;
      appendNews(ary,msg);
    },
    error: function () {
      console.log('error');
      loading = false;
    }
  })
}

//載入日期
function appendDate(data,d){
  let dateTxt = getDateTxt(d);
  $(".landingNewsContent").append('<div class="container" data-date="'+data+'"><article class="newsContent post"><div class="newsDate newsColor">'+dateTxt+'</div><ul class="newsTitle"></ul></article></div>')
  $(".landingNewsContent").append('<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>');
}

//載入新聞連結
function appendNews(arr,obj){
  arr.forEach(function(g,f){
    let t = obj["data"]["data"][g];
    let w = new Date(t["ready_time"].replace(/-/g, "/"));
    let z = w.getFullYear() + "/" + (w.getMonth()+1) + "/" + w.getDate();
    let isIE = window.ActiveXObject || "ActiveXObject" in window
    if(isIE || isMobile()){
      $('[data-date="'+z+'"]').find(".newsTitle").append("<li><a href='"+t["item_url"]+"'>"+t["title"]+"</a></li><span class='newsColor newsCategory'>"+t["category"].toUpperCase()+"</span>");
    }else{
      $('[data-date="'+z+'"]').find(".newsTitle").append("<div class='section'><div class='section__item'><a href='"+t["item_url"]+"' class='sm-link sm-link_text sm-link8' data-sm-link-text='"+t["title"]+"'><span class='sm-link__label'>"+t["title"]+"</span></a><span class='newsColor newsCategory'>"+t["category"].toUpperCase()+"</span></div></div>");
    }
  })

  dayTextChange();
}

//陣列篩選相同值
function GetUnique(inputArray) {
  let outputArray = [];
  for (i in inputArray) {
    if ((jQuery.inArray(inputArray[i], outputArray)) === -1) {
      outputArray.push(inputArray[i]);
    }
  }
  return outputArray;
}

//取得日期文字
function getDateTxt(d){
  var _day = dateCode(d.getDay());
  var _date = d.getDate();
  var _month = monthCode(d.getMonth());
  var _dateTxt = _day+" "+_date+" "+_month;
  return _dateTxt;
}

//取得星期代號
function dateCode(w){
  switch(w){
    case 0:
      return "Sun";
      break;
    case 1:
      return "Mon";
      break;
    case 2:
      return "Tue";
      break;
    case 3:
      return "Wed";
      break;
    case 4:
      return "Thu";
      break;
    case 5:
      return "Fri";
      break;
    case 6:
      return "Sat";
      break;
    defalut:
      return "Sun";
      break;
  }
}

//取得月份代號
function monthCode(m){
  switch(m+1){
    case 1:
      return "Jan";
      break;
    case 2:
      return "Feb";
      break;
    case 3:
      return "Mar";
      break;
    case 4:
      return "Apr";
      break;
    case 5:
      return "May";
      break;
    case 6:
      return "Jun";
      break;
    case 7:
      return "Jul";
      break;
    case 8:
      return "Avg";
      break;
    case 9:
      return "Sep";
      break;
    case 10:
      return "oct";
      break;
    case 11:
      return "Nov";
      break;
    case 12:
      return "Dec";
      break;
  }
}

//每日更新背景顏色
function dayBgChange(){
  let day = new Date().getDay() - 1;
  let color = ["#0093d0","#f8901f","#e82084","#00b333","#000000","#ab1dac","#fac330"];
  let _landingMain = $(".landingMain");
  let _gridContainer = $(".grid-container");
  
  _landingMain.css("background",color[day]);
  _gridContainer.css("background",color[day]);
}

//每日更新文字顏色
function dayTextChange(){
  let day = new Date().getDay() - 1;
  let textColor = ["#00e4ff","#ffde00","#ff91c8","#00ff49","#c6c6c6","#f600ff","#eaff00"];
  let _newsColor = $(".newsColor");
  let _container = $(".container");
  let dayColor = "color:"+(textColor[day]);
  let isIE = window.ActiveXObject || "ActiveXObject" in window
  
  _newsColor.css("color",textColor[day]);
  _container.css("border-bottom-color",textColor[day]);
  if(isIE || isMobile()){
    $(".newsTitle").find('a').hover(
      function(){
        $(".newsTitle").find('a').css("color","#fff");
        $(this).css("color",textColor[day]);
      }
    );
  }else{
    document.styleSheets[0].addRule('.sm-link::before',dayColor);
  }
  
}

//頁面滾動時載入新聞
function init() {
  //setSize();
  $(window).scroll(function() {
    if(isMobile){
      if ($(window).scrollTop() >= ($(document).height() - $(window).height() - 200) && !loading) {
        ajaxLoad();
      }
    }else{
      if ($(window).scrollTop() >= ($(document).height() - $(window).height() -$("#footer").height()) && !loading) {
        ajaxLoad();
      }
    }
  });
}