var isClient=true;

// sungirltv tvlist switch
function switchTvlist(){ 
  $('.sungirltv-nonelimit').toggleClass("leftHide");
  $("html").removeClass('leftMenu');
}
$(".th-tvlist .pt-switch").on("mousedown",switchTvlist);
$(".th-message .tvlistBtn").on("mousedown",function(){
  switchTvlist();
  $(this).addClass("active").siblings().removeClass("active");
});
$(".th-tvMask").on("mousedown",function(){
  $('.sungirltv-nonelimit').addClass("leftHide");
});

// sungirltv msglist switch
$(".th-message .pt-switch").on("mousedown",function(){
  if(window.innerWidth>801){
    $(this).parents('.sungirltv-nonelimit').toggleClass("rightHide");
  }else{
    $(this).parents('.sungirltv-nonelimit').removeClass("rightHide").toggleClass("topHide");
  }
});
$(".th-message .btn-more").on("mousedown",function(){
  $(this).addClass("active").siblings().removeClass("active");
  $('.sungirltv-nonelimit').addClass("hideList");
});
$(".th-message .msg").on("mousedown",function(){
  $(this).addClass("active").siblings().removeClass("active");
  $('.sungirltv-nonelimit').removeClass("hideList");
});


function setliveMenu(){
  if($(".main-sungirltv").length>0){
    $('.sungirltv-nonelimit').addClass("leftHide");
    if(window.innerWidth<801){
      $('.sungirltv-nonelimit').removeClass("rightHide");
    }    
  }  
}
setliveMenu();
var restTimer;
$(window).resize(function(){
  clearTimeout(restTimer);
  restTimer = setTimeout(setliveMenu,20)
}).resize();



// sungirltv msg link 
function msgLink(Str){
  var trimStr = Str.trim();
  var reg=/(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&:/~\+#]*[\w\-\@?^=%&/~\+#])?/;
  var url = reg.exec(trimStr);
  if(!!url){
    var text = trimStr.split(url[0]);    
    if(text[0]==""){
      return '<a href="'+ url[0] +'" target="_blank">'+ url[0]+'</a>'+text[1];
    }else{
      return text[0]+'<a href="'+ url[0] +'" target="_blank">'+ url[0]+'</a>'+text[1];
    }     
  }else{
    return trimStr;
  }
} 

// sungirltv msglist scroll bottom
function scrollmsglist(e){
  var list = $(".th-message .md-list");
  var scrollVal = list.scrollTop();
  var msg_H = 0;
  $(".th-message .md-list >li").each(function(){msg_H+= $(this).innerHeight();});
  if( !e && (msg_H-scrollVal > 800) ){
   return;  
  }
  $(".th-message .md-list").stop().animate({scrollTop:msg_H});
}

function scrollBtn(){
  var list = $(".th-message .md-list");
  var scrollVal = list.scrollTop();
  var msg_H = 0;
  var list_H = list.height();
  list.find("li").each(function(){msg_H+= $(this).innerHeight()});
  if((msg_H-scrollVal) == list_H || (msg_H-scrollVal)-20 < list_H){
    $(".scrollBtn").hide();
  }else{
    $(".scrollBtn").show();
  }
}

//AJAX 傳送聊天訊息
function ajaxSendChatMessage(message, atName){
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  
  $.ajax({
      url: '/tv/ajaxSendChatMessage',
        type: 'POST',
        data: {_token: CSRF_TOKEN, message: message, atName: atName},
        dataType: 'JSON',
        success: function (data) { 
          //alert(JSON.stringify(data));
            //console.log(data.status);
            switch(data.status) {
              case 'success':
                scrollmsglist(1);//新訊息生成後才呼叫此function來滾動內容高度
                break;
              case 'errorLogin':
                //alert(data.msg);
                window.location.href = "login";
                break;
              case 'errorMessage':
                alert(data.msg);
                break;
            }
            
        },
        error: function(jqXHR) {
          alert("發生錯誤: " + jqXHR.status);
        }
    }); 
}

//AJAX 取得線上人數
function ajaxGetOnlineGuests(){
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      
    $.ajax({
      url: '/tv/ajaxGetOnlineGuests',
        type: 'POST',
        data: {_token: CSRF_TOKEN},
        dataType: 'JSON',
        success: function (data) { 
          //alert(JSON.stringify(data));
            //console.log(data.count);
          //var count = data.count + 1;
            var count = data.count;
            $('#online_guests_1').html(count);
          $('#online_guests_2').html(count);
          setTimeout('ajaxGetOnlineGuests( )', 60000);//1000是1秒
        },
        error: function(jqXHR) {
          alert("發生錯誤: " + jqXHR.status);
          
        }
    }); 
}

// 直播video 聲音switch
if($("#liveVideo").length>0){
  var _video = $("#liveVideo"); 
  _video.on("volumechange",function(){
    if(_video.get(0).muted){
      $('#btn-voice').addClass('muted');
    }else{
      $('#btn-voice').removeClass('muted');
    } 
  });

  $('#btn-voice').on('click',function(){
    if(_video.get(0).muted){
      _video.get(0).muted = false;
      $('#btn-voice').removeClass('muted');
    }else{
      _video.get(0).muted = true;
      $('#btn-voice').addClass('muted');
    }                    
  });
}


//---canvas player---start-----------------回放用canvas
// 換算影片秒數
var vmDatas = {
  "videos": [
    {
      "id": "11",
      "name":"中文名稱中文名稱1",
      "video_url": "http://mv.sungirlbaby.com/video/t1.mp4",
      // "seconds": 454
      "seconds":40
    },
    {
      "id": "12",
      "name":"中文名稱中文名稱2",
      "video_url": "http://mv.sungirlbaby.com/video/t2.mp4",
      // "seconds": 69
      "seconds":30
    },
    {
      "id": "12",
      "name":"中文名稱中文名稱3",
      "video_url": "http://mv.sungirlbaby.com/video/t3.mp4",
      // "seconds": 4341
      "seconds":50
    },
    {
      "id": "13",
      "name":"中文名稱中文名稱4",
      "video_url": "http://mv.sungirlbaby.com/video/t4.mp4",
      "seconds": 27
    }
  ]
}
var startDate = new Date();
    startDate.setDate(1);
    startDate.setHours(0);
    startDate.setMinutes(0);
    startDate.setSeconds(0);
var videoData;
var voiceStatus = false;
var isMobile = false;
function setVideoData(data){ return videoData = data;}
if(isClient){
  setVideoData(vmDatas);
}
var mobiles = new Array
    (
      "midp", "j2me", "avant", "docomo", "novarra", "palmos", "palmsource",
      "240x320", "opwv", "chtml", "pda", "windows ce", "mmp/",
      "blackberry", "mib/", "symbian", "wireless", "nokia", "hand", "mobi",
      "phone", "cdm", "up.b", "audio", "sie-", "sec-", "samsung", "htc",
      "mot-", "mitsu", "sagem", "sony", "alcatel", "lg", "eric", "vx",
      "NEC", "philips", "mmm", "xx", "panasonic", "sharp", "wap", "sch",
      "rover", "pocket", "benq", "java", "pt", "pg", "vox", "amoi",
      "bird", "compal", "kg", "voda", "sany", "kdd", "dbt", "sendo",
      "sgh", "gradi", "jb", "dddi", "moto", "iphone", "android",
      "iPod", "incognito", "webmate", "dream", "cupcake", "webos",
      "s8000", "bada", "googlebot-mobile"
    );
var uaLc = navigator.userAgent.toLowerCase();
  for (var i = 0; i < mobiles.length; i++) {
    if (uaLc.indexOf(mobiles[i]) > 0) {
        isMobile = true;             
        break;
    }
  }

var tvObj = {
  app : null,
  cRender : null,  
  videoEl : document.createElement('video'),
  videoSource : document.createElement("source"),
  timer1 : null,
  timer2 : null,
  texture : null,
  videoSprite : null,
}


function canvasPlayer(){
      tvObj.app = new PIXI.Application({ transparent: true });
      tvObj.cRender = new PIXI.CanvasRenderer();
  var canvasVideo = document.getElementById("canvasVideo"); 
      canvasVideo.appendChild(tvObj.app.view);
  var untilnowTime = (((new Date() - new Date(startDate))/1000).toFixed(0))/1;   
  var ua = window.navigator.userAgent;
  var positionTime = 0;
  var videoTime = 0;
  var videoStartTime = [0];
  var nowIndex;
  var starTime;
  var switchStatus = false;
      tvObj.app.stage.interactive = true;
      tvObj.app.stage.buttonMode = true;
      
  // 處理load進頁面顯示目前播放的該影片及該秒數    
  for(var i=0, length = videoData.videos.length; i<length; i++){
    videoTime += (videoData.videos[i].seconds/1);
    if(i<(length-1)){
      videoStartTime.push(videoTime+1);
    }
  }
  positionTime = untilnowTime%videoTime;
  for(var j=0; j< videoStartTime.length;j++){
    if(positionTime >= videoStartTime[j] && positionTime < videoStartTime[j+1]){ 
      nowIndex = j;
    }
  }
  if(positionTime >= videoStartTime[videoStartTime.length-1]){
    nowIndex = videoStartTime.length-1;
  }    
  starTime = (positionTime - videoStartTime[nowIndex]);


  //進頁面後連續撥放
  function unceasingPlay(index){
    var vi,t;
    clearTimeout(tvObj.timer1);
    vi = (index%videoData.videos.length);
    t = (videoData.videos[vi].seconds)*1000;  
    starTime = 0;
    videoPlay(vi); 
    tvObj.timer2 = setInterval(function(){
      destroyVideo();
      clearInterval(tvObj.timer2);
      unceasingPlay((vi+1));
    }, t);
  }

  
  function videoPlay(index){
    $(".th-view .newest span").text(videoData.videos[index].name);
    tvObj.texture = PIXI.Texture.fromVideo(videoData.videos[index].video_url);
    tvObj.texture.baseTexture.source.load();
    tvObj.texture.baseTexture.source.setAttribute('muted','');
    tvObj.texture.baseTexture.source.setAttribute('autoplay','');
    tvObj.texture.baseTexture.source.addEventListener('play',function(){
      tvObj.texture.baseTexture.source.currentTime = starTime;
    });      
    tvObj.videoSprite = new PIXI.Sprite(tvObj.texture);
    tvObj.videoSprite.width = tvObj.app.renderer.width;
    tvObj.videoSprite.height = tvObj.app.renderer.height;
    tvObj.app.stage.addChild(tvObj.videoSprite);    
    tvObj.texture.baseTexture.source.muted = voiceStatus = true;
    $("#canvasVoiceBtn").addClass('muted').removeClass('nonemute');
    $('#btn-voice').addClass('muted');
    //解決IE11以上的問題 
    if(ua.indexOf('Trident/7.0') >0 || ua.indexOf('Edge/')>0){
      tvObj.app.renderer = tvObj.cRender;
      canvasVideo.appendChild(tvObj.cRender.view);    
    } 
    // 解決IOS 10 webGL的問題
    if(navigator.userAgent.match(/OS 10/i)){
      tvObj.app.renderer = tvObj.cRender;
      canvasVideo.appendChild(tvObj.cRender.view);   
    }
  }

  function destroyVideo(){
    tvObj.texture.destroy(true);
    tvObj.videoSprite.destroy(true);
    tvObj.texture.baseTexture = null;
    tvObj.videoSprite = null;
    tvObj.texture = null;
  } 
  
  videoPlay(nowIndex);
  tvObj.timer1 = setTimeout(function(){
    destroyVideo();
    unceasingPlay(nowIndex+1);
  },((videoStartTime[nowIndex] + videoData.videos[nowIndex].seconds/1 - positionTime)*1000));   
  
  // switchVioce
  if(!isMobile){
    tvObj.app.stage.on('pointerdown',function(){
      if(!switchStatus){
        switchStatus = true;
        if(window.innerWidth<980){
          $("#canvasVoiceBtn").addClass('show');
          var timer3 = setTimeout(function(){
            $("#canvasVoiceBtn").removeClass('show');
            clearTimeout(timer3);
            switchStatus = false;
          },2000); 
        }else{
          $("#canvasVoiceBtn").removeClass('show');
          switchStatus = false;
        }
        if(!tvObj.texture.baseTexture.source.muted){
          tvObj.texture.baseTexture.source.muted = voiceStatus = true;
          $("#canvasVoiceBtn").addClass('muted').removeClass('nonemute');
          $('#btn-voice').addClass('muted');
        }else{
          tvObj.texture.baseTexture.source.muted = voiceStatus = false;
          $("#canvasVoiceBtn").addClass('nonemute').removeClass('muted');
          $('#btn-voice').removeClass('muted');
        } 
      }
    });
  }

  function canvasVoice(){
    if(tvObj.texture.baseTexture.source.muted){
      tvObj.texture.baseTexture.source.muted = false;
      $('#btn-voice').removeClass('muted');
    }else{
      tvObj.texture.baseTexture.source.muted = true;
      $('#btn-voice').addClass('muted');
    }                    
  }
  $('#btn-voice').on('click',canvasVoice);

}

if($(".canvasCont").length>0){
  if(isClient){
    canvasPlayer();
  }
}


//---canvas player----end----------------- 

// switch fixMsg
function switchFixMsg(){
  $(".md-fixMsg").toggleClass('fixMsgHide')
}
// switchFixMsg()
$(".md-fixMsg .closeBtn").on('click',switchFixMsg)

function getList(array){
  array.sort(); 
  var re=[array[0]];
    for(var i = 1; i < array.length; i++ ){
    if( array[i] !== re[re.length-1])
    {
      re.push(array[i]);
    }
  }
  return re;
} 


//聊天回復功能
var at_list = [];
var input = $('#chatText');
var numberMsg;
var atName = "";
var clearMsg = function(f){
  f();
  at_list = [];
  atName = "";
}

function removeAtName(){
  $(".md-msgCont").find('span').remove();
  $(".md-msgCont").css('padding-left',0);
  $(".chatAtNum").hide();
}

function addAtName(name,arr){
  arr.push(name);
  $(".md-msgCont").append('<span class="userAt">@'+name+'</span>');
  $(".md-msgCont").css('padding-left',$(".md-msgCont").find('span').width()+20);
}

function toggleChatList(e){
  if(!e){
    $(".chatAtList").show();
  }else{
    $(".chatAtList").hide();
  }
  event.stopPropagation();
}

function getUserNum(arr){
  getList(arr).forEach(element => $(".chatAtList").append('<span>@'+element+'</span>'));
  $(".userAt").click(function(event){
    toggleChatList(0);//開啟標註列表
  })
  $(".main-content").click(function(event){
    if($('.chatAtList').is(':visible')){
      toggleChatList(1);
    }
  })
}

function getNumTag(arr){
  $(".chatAtNum").text(arr.length).show();
  $(".chatAtNum").css('left',$(".md-msgCont").find('span').width()+10);
}

function getUserName(o){
  //標註其他用戶
  var at_name = $(o).text();
  if(at_list.length>0){
    if(!(at_list.includes(at_name)) && !(at_list.length>9)){
      removeAtName();
      addAtName(at_name,at_list);
      getNumTag(at_list);
      getUserNum(at_list);
    }else if((at_list.length>9)){
      alert('一次最多只能標註十人喔');
    }
  }else{
    removeAtName();
    addAtName(at_name,at_list);
    getUserNum(at_list);
  }
}

$(".chatAtNum").click(function(event){
  toggleChatList(0);//開啟標註列表
})

$(".chatAtList").click(function(event){ 
  toggleChatList(1);//關閉標註列表
})

// msg內容是否有連結以及內容高度滾動
$(".md-msgCont button").on("click",function(){
  numberMsg = msgLink($('.th-message .md-msgCont input').val());
  at_list.forEach(element => atName += "@"+element+'\xa0' );
  //取得標註名單 - atName;
  $('.th-message .md-msgCont input').val("");

  if(isClient){
    scrollmsglist(1);//新訊息生成後才呼叫此function來滾動內容高度
  }else{
    ajaxSendChatMessage(numberMsg, atName);
  }

  clearMsg(removeAtName);
});

$('.th-message .md-msgCont input').on('keydown',function(e){
  switch(e.keyCode)
  {
    case 13:
      $(".md-msgCont button").click();
      break;
    case 8:
      if(!$(this).val()){
          clearMsg(removeAtName);
      }
      break;    
  }
});

window.onload = function() {
  //開啟頁面先置底
  scrollmsglist(1);
  $(".th-message .md-list").scroll(function(event) {
    scrollBtn();
  });
};
